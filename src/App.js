import './App.css';
import Modal from './components/modals';
import { Component } from 'react';
import Button from './components/buttons';
import { modalWindowDeclarations } from './components/modals/modal-data';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      isOpenedModal: false,
      modalContent: {}
    }
  }

  openModal = (modalID, bgColor) => {
    const modalDeclaration = modalWindowDeclarations.find(item => item.id === modalID);
    if (!modalDeclaration) {
      this.closeModal()
    } else {
      this.setState({isOpenedModal: true, modalContent: {...modalDeclaration, bgColor}})
    }
  }

  closeModal = () => {
    this.setState({isOpenedModal: false, modalContent: {}})
  }

  render(){
    return(
      <>
        <div className='btn_container'>
          <Button className={'btn btn_first'} backgroundColor={'crimson'} text='Open First Modal' 
            onClick={() => this.openModal('modalID1', 'crimson')}/>
          <Button className={'btn btn_second'} backgroundColor={'deepskyblue'} text='Open Second Modal' 
            onClick={() => this.openModal('modalID2', 'deepskyblue')} />
        </div>

        <Modal
          show={this.state.isOpenedModal}
          header={this.state.modalContent.title} 
          closeButton={true}
          text={this.state.modalContent.description} 
          bgColor={this.state.modalContent.bgColor} 
          onClose={() => this.closeModal()} 
        />
      </>
    )
  }
} 


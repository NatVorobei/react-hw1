import { Component } from "react";
import './button.scss';

export default class Button extends Component{
    // constructor(props){
    //     super(props);
    // }

    render(){
        const { backgroundColor, text, onClick, className } = this.props;
        return(
            <>
                <button styles={{backgroundColor: backgroundColor}} className={className} onClick={onClick}>{text}</button>
            </>
        )
    }
}
import { Component } from "react";
import Button from "../buttons";
import '../modals/modal.scss';


export default class Modal extends Component {
    render(){
        const {show, header, closeButton, text, bgColor, onClose} = this.props; 

        if(!show){
            return null;
        }

        return (
            <>
                <div className="overlay" onClick={onClose}></div>
                <div className={"modal " + (bgColor ? bgColor : '')}>
                    <div className="header">
                        <h3 className="title_header">{header}</h3>
                        {closeButton && <Button className={'chrest'} text='✕' onClick={onClose}/>}
                    </div>
                    
                    <div className="modal_text">
                        <p>{text}</p>
                    </div>

                    <div className="modal_btns">
                        <Button className={"btn modal_btn"} text='Ok' onClick={onClose}/>
                        <Button className={"btn modal_btn"} text='Cancel' onClick={onClose}/>
                    </div>
                </div>
            </>
        )
    }
} 
export const modalWindowDeclarations = [
    {
      id: 'modalID1',
      title: "Do you want to delete this file?",
      description: 'Once you delete this file, it won\'t be possible to undo this action. Are you sure you want to delete it?'
    },
    {
      id: 'modalID2',
      title: "Do you want to close this window?",
      description: 'Once you close this window, it will be closed'
    }
];